package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
	"sync"
	"time"
)

/*
upcheck
=======
Polls a supplied list of endpoints to make sure they return 2xx within a
given timeframe.  Program will exit with a zero status on success, or
non-zero if one or more services is down, or not responding to GET
requests.

Usage:
  -timeout int
        Max seconds before all urls have become available (default 15)
  -urlSource string
        File containing urls to check, or '-' for stdin (default "-")
*/

// default error if server unreachable
const (
	defaultFailureStatus  = 599
	defaultFailureMessage = "599 Network Connect Timeout Error"
)

var (
	flagUrlSource  = flag.String("urlSource", "-", "File containing urls to check, or '-' for stdin")
	flagTimeoutSec = flag.Int("timeout", 15, "Max seconds before all urls have become available")
)

type UrlResponse struct {
	Url        string
	StatusCode int
	Status     string
}

func readLines(r io.Reader) ([]string, error) {
	data, err := ioutil.ReadAll(r)
	return strings.Split(string(data), "\n"), err
}

func readUrls(path string) ([]string, error) {
	var r io.Reader
	if path == "-" {
		r = os.Stdin
	} else {
		f, err := os.Open(path)
		if err != nil {
			return []string(nil), err
		}
		r = f
	}
	return readLines(r)
}

func checkUrlsWithTimeout(urls []string, timeout time.Duration) ([]UrlResponse, []UrlResponse) {
	var w sync.WaitGroup
	var responses = make(chan UrlResponse, len(urls))

	for _, u := range urls {
		u = strings.Trim(u, " \t\r")
		if u == "" {
			continue
		}
		_, err := url.Parse(u)
		if err != nil {
			continue
		}
		w.Add(1)
		go func(u string, timeout time.Duration, r chan UrlResponse) {
			start := time.Now()
			resp, err := http.Get(u)
			for time.Since(start) < timeout && (err != nil || resp.StatusCode/100 != 2) {
				time.Sleep(time.Second)
				resp, err = http.Get(u)
			}
			code := defaultFailureStatus
			status := defaultFailureMessage
			if resp != nil {
				code = resp.StatusCode
				status = resp.Status
			}
			r <- UrlResponse{Url: u, StatusCode: code, Status: status}
			w.Done()
		}(u, timeout, responses)
	}
	w.Wait()

	var good, bad []UrlResponse
	l := len(responses)
	for i := 0; i < l; i++ {
		resp := <-responses
		fmt.Printf("- Url: %s\n", resp.Url)
		fmt.Printf("  Status: %d (%s)\n", resp.StatusCode, resp.Status)
		if resp.StatusCode/100 != 2 {
			bad = append(bad, resp)
		} else {
			good = append(good, resp)
		}
	}

	return good, bad
}

func main() {
	flag.Parse()
	urls, err := readUrls(*flagUrlSource)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		os.Exit(1)
	}
	if len(urls) == 0 {
		fmt.Println("No urls.")
		os.Exit(0)
	}
	_, bad := checkUrlsWithTimeout(urls, time.Duration(*flagTimeoutSec)*time.Second)
	// Exit status will contain # of failures
	os.Exit(len(bad))
}
