# UPCHECK

Upcheck is a small Go utilitly which will ensure that a list of urls are
returning a 2xx status, within an a given timeframe.

It's main purpose is to ensure that within a local integration test,
services are available and responding _before_ starting the tests.  This
should replace arbitrary "sleep XX" commands in the scripts.

Upcheck will check all of the urls concurrently to make sure they are up
and will retry each until it returns 2xx or the timeout has expired.

## General usage

```
upcheck -timeout <seconds> -urlSource <file|->

Usage:
  -timeout int
        Max seconds before all urls have become available (default 15)
  -urlSource string
        File containing urls to check, or '-' for stdin (default "-")
```

## Usage within a make.sh

### Installation of the binary
Add the following to your script: -

```go get -u bitbucket.org/aprilayres/upcheck```

### Calling upcheck

After both pubsub emulator (if used), and your service have been started, but
before the integration tests are run, add the following: -

```shell
upcheck <<EOF
    http://localhost:8080/_ah/health
    http://${PUBSUB_EMULATOR_HOST}/v1/projects/localhost/subscriptions
EOF
```

The program will output a summary of it's results of polling the urls.

```
- Url: http://localhost:8010/v1/projects/localhost/subscriptions
  Status: 200 (200 OK)
- Url: http://localhost:8080/_ah/health
  Status: 200 (200 OK)
```

```$?``` will be non-zero if one or more of the services did not respond in time.

```shell
if [ $? -ne 0 ]; then
    # exit script here, complain about life
fi
```

